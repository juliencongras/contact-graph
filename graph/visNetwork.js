fetch('../bdd/contacts.json')
    .then((response) => response.json())
    .then((tab) => {
        monTab = tab;

        var nodesTab = []
        for (var i = 0; i < monTab.people.length; i++){
          var nodesPeopleId = monTab.people[i].id;
          var nodesPeopleName = monTab.people[i].name;
        
          nodesTab.push({
            id: nodesPeopleId,
            label: nodesPeopleName
          });
        }

        var nodes = new vis.DataSet(nodesTab)

        var edgesTab = []
        for (var i = 0; i < monTab.contact.length; i++){
          var edgesContactPersonId = monTab.contact[i].personId;
          var edgesContactInContactWith = monTab.contact[i].inContactWithId;
        
          edgesTab.push({
            from: edgesContactPersonId,
            to: edgesContactInContactWith
          });
        }

        var edges = new vis.DataSet(edgesTab)

        // create a network
        var container = document.getElementById("mynetwork");
        var data = {
          nodes: nodes,
          edges: edges,
        };
        var options = {};
        var network = new vis.Network(container, data, options);
    })