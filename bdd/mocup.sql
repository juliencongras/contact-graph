-- Créer un fichier mocup.sql
-- Il doit créer 50 personnes et 75 contacts entre ces personnes.
use contactGraph;
delete from `people`;
delete from `contact`;

INSERT INTO people(name) VALUES
("Sunshine"), ("Peppa Pig"), ("Red Velvet"), ("Chance"), 
("Einstein"), ("Starbuck"), ("Amiga"), ("Flower"), ("PB&J"), ("Tough Guy"), ("Foxy"), 
("Robin"), ("Cumulus"), ("Cheese"), ("Sourdough"), ("Ms. Congeniality"), ("Ghoulie"), 
("Sherlock"), ("Bambino"), ("Foxy Lady"), ("MomBod"), ("Tater Tot"), 
("Dino"), ("Little Bear"), ("Lil Girl"), ("Smarty"), ("Chickie"), ("Grease"), 
("Muscles"), ("Weiner"), ("Donut"), ("Stud"), ("Chuckles"), ("Chef"), ("Gordo"), 
("4-Wheel"), ("Queenie"), ("Friendo"), ("Ami"), ("Amour"), ("Chico"), ("Babs"), 
("Bean"), ("Diet Coke"), ("Frogger"), ("Green Giant"), ("Turkey"), ("Terminator"), 
("Tickles"), ("Dropout");

INSERT INTO `contact` (personId, inContactWithId) VALUES 
(1, 2), (1, 5), (1, 3), (2, 43), (3, 8), (3, 37), (4, 1), (4, 23), (5, 40), (6, 10), (6, 17), 
(7, 25), (8, 44), (9, 19), (10, 11), (11, 37), (12, 48), (13, 22), (14, 40), (15, 17), (16, 23), 
(17, 39), (18, 29), (19, 34), (20, 23), (21, 46), (22, 30), (23, 47), (24, 37), (25, 28), 
(26, 34), (27, 9), (28, 32), (29, 3), (30, 1), (30, 32), (33, 47), (34, 5), (35, 8), (36, 37), 
(38, 29), (39, 42), (40, 25), (41, 43), (42, 2), (43, 9), (44, 39), (45, 1), (46, 36), 
(47, 27), (48, 3), (49, 1), (31, 4), (32, 13), (31, 1), (1, 23), (3, 12), (7, 50), (10, 14), 
(15, 16), (17, 18), (19, 45), (20, 24), (21, 44), (22, 33), (23, 40), (24, 45), (25, 49), 
(26, 30), (27, 28), (28, 30), (29, 36), (30, 42), (31, 29), (31, 49);