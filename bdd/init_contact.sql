drop database if exists contactGraph;
create database contactGraph;
use contactGraph;

create table `people` (
    id int not null auto_increment primary key,
    name varchar(255)
);

create table `contact` (
    personId int not null,
    inContactWithId int not null,
    FOREIGN KEY (PersonId) REFERENCES people(id),
    FOREIGN KEY (inContactWithId) REFERENCES people(id)
);

drop USER if exists 'tousStopAntiCovid'@'127.0.0.1';
CREATE USER 'tousStopAntiCovid'@'127.0.0.1' IDENTIFIED BY 'admin123';
GRANT ALL PRIVILEGES ON contactGraph.* TO 'tousStopAntiCovid'@'127.0.0.1';
ALTER USER 'tousStopAntiCovid'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'admin123';