#Contexte du projet

Pour une raison obscure la Gouvernance* d'EE** veut un visuel sur les contacts qu'ont ses employés*** les uns avec les autres.

Les chefs du bonheur**** de l'inintelligence économique***** ont mis la main sur le Trello d'une application qui fait déjà ça.

Vous devez dupliquer le Trello et développer l'application.

​*Direction tyranique
**Evil Empire
***soit toute la population mondiale
****espions
*****espionage industriel

#Modalités pédagogiques

Vous pouvez travaillez seul, ou en groupe.

En fonction de la taille du groupe vous avez plus ou moins de temps.

1 → 5 jours
2 → 3 jours
3 → 2 jours
4 → 5 jours
6 → 1 jour

#Modalités d'évaluation

Présentation orale devant le DGA*

Vous déroulerez le UC suivants :
1 ouvrir la page permettant d'ajouter une personne.
2 ajouter 3 personnes : "Bob", "Keanu" et "Chuck".
3 aller sur la page des contacts
4 créer un contact entre "Bob" et "Chuck" et un autre entre "Bob" et lui même.
5 aller sur la page du graphe.
6 lancer la collection post man.
7 retourner sur la page du graphe.
8 ajouter les données mockup à la BDD.
9 retourner sur la page du graphe.

*Directeur Général Absolu.

#Livrables

Un repo git public en ligne avec TOUT dedans (sauf les images ! ).