<?php
include_once "../pdo.php";

$req = $pdo->prepare('DELETE FROM contact;');
    $req->execute();
$req = $pdo->prepare('ALTER TABLE contact AUTO_INCREMENT = 1;');
    $req->execute();
$req = $pdo->prepare('DELETE FROM people;');
    $req->execute();
$req = $pdo->prepare('ALTER TABLE people AUTO_INCREMENT = 1;');
    $req->execute();

header('location: delete.php')
?>