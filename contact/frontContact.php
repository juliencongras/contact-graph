<?php
include_once "../pdo.php";
$req = $pdo->query('select * from people;');
$people = $req->fetchAll();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Créer contact entre deux personnes</title>
    <link rel="stylesheet" href="frontContact.css">
</head>
<body>
    <?php include_once "../header.php" ?>
    <form action="contactController.php" method="post">
        <select name="personOne" id="personOne" size="5">
            <?php
            foreach($people as $x){?>
                <option value="<?= $x['id'] ?>"><?= $x['name'] ?></option>
            <?php }
            ?>
        </select>
        <select name="personTwo" id="personTwo" size="5">
            <?php
            foreach($people as $x){?>
                <option value="<?= $x['id'] ?>"><?= $x['name'] ?></option>
            <?php }
            ?>
        </select>
        <button>CONTACT</button>
    </form>
</body>
</html>