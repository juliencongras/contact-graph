<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contacts Graph</title>
    <script
      type="text/javascript"
      src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"
    ></script>
    <link rel="stylesheet" href="contactsGraph.css">
</head>
<body>
    <?php include_once "../header.php" ?>
    <h1>Graphique de contacts</h1>
    <div id="mynetwork"></div>
<script src="visNetwork.js"></script>
</body>
</html>